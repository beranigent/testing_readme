# BackbaseAssignment

## Getting Started

This project is fulfilling all requirements that are written on the Instructions for the test file so is a solution of a test assignment given by Backbase.
Project is developed for Android platform by using Java as programming language. 

### Intro

The project Architecture is MVP. 
We have 3 contracts: Home, Map and About contracts.

Home is handling Search and City implementation, Map is handling interactions and map behavior and About is handling about informations about Backbase company. Although the assignment says “[...] opens an information screen about the selected city”, the provided code/data always display the Backbase company information.

### Search

Loading time of the app is long because it prepares the data to be in a fast-search format. 
Reading json file is executed asynchronously by using AsyncTask. 
The reason why it takes a bit while is that once that we get the cities array from json and then we are creating a HashMap which is contains first letter of alphabet which is fetched from the first letter of existing cities name.

Once that user is going to write a character for example ‘A’, we are going to find on HashMap the key that contains cities that start with that letter. This will affect that the results to return only from one bucket, in this case from bucket ‘A’.

```
final HashMap<Character, List<City>> alphaCitiesMap;
filterHelper.getFilteredCities(“A", alphaCitiesMap);

```

After we find the alphabet bucket then we are going to iterate over the cities on that bucket.

```
if (cityModel.getCityAndCountry().toLowerCase().indexOf(query) == 0) {
filteredList.add(city);
}

```

### Orientation

The application works with different UX in Landscape. When the screen is rotated from Portrait to Landscape then we are going to change the xml for landscape.

The data are handled by calling setRetainInstance(true); on the SearchFragment which let the Fragment holds the data but just to re-create the view side.

On orientation you can click on any city and the city will show up on its coordination on the right side on google maps.


## Tests

The project includes UI & Unit Tests. 

#Unit Tests
UnitTests are covered by testing Models and SearchFilter, in this case I didn’t test the Presenter because I didn’t have a lot of time and I’m using AsyncTask on the Presenter which is kind a tricky to test it without any library like Roboelectric or PowerMockito. 

#UITests 
UiTests are covered by testing SearchFragment and AboutActivity.

### Improvements
The search speed can probably be further improved by a Tree implementation of the data where each character is a node.
About screen data and code can be changed to display the information about the clicked city.
